# EM-RGB

ESP-8266 MQTT RGB controller.

## Getting Started

This project was built in the [PlatformIO IDE](http://platformio.org/). As such, cloning the repo and opening it up in should get you up and running. If you instead are running this in the Arduino IDE, some changes need to be made.

### Prerequisites

The following libraries are needed. This should be taken care of if you are using [PlatformIO IDE](http://platformio.org/). If you are using the Arduino IDE these instructions will show you how to install 3rd party libs.

* [ArduinoJson](https://github.com/bblanchon/ArduinoJson)
* [AsyncMqttClient](https://github.com/marvinroger/async-mqtt-client)
* [ESPAsyncTCP](https://github.com/me-no-dev/ESPAsyncTCP)
* [ESPAsyncWifiManager](https://github.com/alanswx/ESPAsyncWiFiManager)
* [ESPAsyncWebServer](https://github.com/me-no-dev/ESPAsyncWebServer)
* [Button](https://github.com/JChristensen/Button)

### Installing

A step by step series of examples that tell you have to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo
