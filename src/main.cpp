#include <Arduino.h>
#include <ArduinoOTA.h>
#include <ArduinoJson.h>
#include <DNSServer.h>
#include <ESP8266mDNS.h>
#include <ESP8266WiFi.h>
#include <ESPAsyncWebServer.h>
#include <ESPAsyncWiFiManager.h>
#include <FS.h>
#include <Hash.h>
#include <AsyncMqttClient.h>
#include <RGBLed.h>
#include <config.h>
#include <Button.h>

const size_t bufferSize = JSON_OBJECT_SIZE(3) + JSON_OBJECT_SIZE(4) + 70;

RGBLed led(R_PIN, G_PIN, B_PIN);
Button button(BUTTON_PIN, false, true, 20);
AsyncMqttClient mqttClient;
AsyncWebServer server(80);
DNSServer dns;
bool shouldSave = false;

//default values
char device_name[64] = DEVICE_NAME;
char pub_topic[72] = "";
char sub_topic[72] = "";
char mqtt_server[64] = SERVER;
char mqtt_port[8] = PORT;
char mqtt_username[64] = USERNAME;
char mqtt_password[64] = PASSWORD;

void onConnect(bool sessionPresent){
  DEBUGLN("Connected to mqtt broker");

  mqttClient.subscribe(sub_topic, 0);
}

void sendState(){
  DynamicJsonBuffer jsonBuffer(bufferSize);
  JsonObject& root = jsonBuffer.createObject();
  ledState state = led.getState();
  root["state"] = (state.on) ? CMND_ON : CMND_OFF;
  root["brightness"] = state.brightness;

  char buff[root.measureLength() + 1];
  root.printTo(buff, sizeof(buff));

  mqttClient.publish(pub_topic, 0, true, buff);
}

void onMessage(char* topic, char* payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total){
  if (!strcmp(topic, sub_topic)){
    DynamicJsonBuffer jsonBuffer(bufferSize);
    JsonObject& json = jsonBuffer.parseObject(payload);
    if (!json.success()){
      DEBUGLN("Json parsing failed.");
      return;
    }

    if(json.containsKey("brightness"))
      led.setBrightness(json["brightness"]);
    if(json.containsKey("color"))
      led.setColour(json["color"]["r"], json["color"]["g"], json["color"]["b"]);
    if(json.containsKey("state")){
      if(!strcmp(json["state"], CMND_ON)) led.on();
      if(!strcmp(json["state"], CMND_OFF)) led.off();
    }
  }
}

bool loadConfig(){
    //read configuration from FS json
  DEBUGLN("mounting FS...");

  if (SPIFFS.begin()) {
    DEBUGLN("mounted file system");
    if (SPIFFS.exists("/config.json")) {
      //file exists, reading and loading
      DEBUGLN("reading config file");
      File configFile = SPIFFS.open("/config.json", "r");
      if (configFile) {
        DEBUGLN("opened config file");
        size_t size = configFile.size();
        // Allocate a buffer to store contents of the file.
        std::unique_ptr<char[]> buf(new char[size]);

        configFile.readBytes(buf.get(), size);
        DynamicJsonBuffer jsonBuffer;
        JsonObject& json = jsonBuffer.parseObject(buf.get());
        #ifdef DEBUG
        DEBUGLN("");
        json.prettyPrintTo(Serial);
        DEBUGLN("");
        #endif
        if (json.success()) {
          DEBUGLN("\nparsed json");

          strcpy(mqtt_server,   json["mqtt_server"]);
          strcpy(mqtt_port,     json["mqtt_port"]);
          strcpy(mqtt_username, json["mqtt_username"]);
          strcpy(mqtt_password, json["mqtt_password"]);
          strcpy(device_name,   json["device_name"]);
          return true;
        } else {
          DEBUGLN("failed to load json config");
          return false;
        }
      }
    }
  } else {
    DEBUGLN("failed to mount FS");
    return false;
  }
  //end read
}

void setupWifi(){
    AsyncWiFiManager wifiManager(&server, &dns);
    AsyncWiFiManagerParameter custom_mqtt_server("server", "mqtt server", mqtt_server, 64);
    AsyncWiFiManagerParameter custom_mqtt_port("port", "mqtt port", mqtt_port, 8);
    AsyncWiFiManagerParameter custom_mqtt_username("username", "username", mqtt_username, 64);
    AsyncWiFiManagerParameter custom_mqtt_password("password", "password", "", 64);
    AsyncWiFiManagerParameter custom_device_name("deviceName", "device name", device_name, 64);

    wifiManager.addParameter(&custom_mqtt_server);
    wifiManager.addParameter(&custom_mqtt_port);
    wifiManager.addParameter(&custom_mqtt_username);
    wifiManager.addParameter(&custom_mqtt_password);
    wifiManager.addParameter(&custom_device_name);
    wifiManager.setSaveConfigCallback([]{shouldSave = true;});

    #ifdef DEBUG
    //wifiManager.resetSettings();
    wifiManager.autoConnect();
    #else
    String ssid = "ESP" + String(ESP.getChipId());
    wifiManager.autoConnect(ap_ssid.c_str(), AP_PASSWORD)
    #endif


    strcpy(mqtt_server,   custom_mqtt_server.getValue());
    strcpy(mqtt_port,     custom_mqtt_port.getValue());
    strcpy(mqtt_username, custom_mqtt_username.getValue());
    strcpy(device_name,   custom_device_name.getValue());
    if(strcmp(custom_mqtt_password.getValue(), "")){
      strcpy(mqtt_password, custom_mqtt_password.getValue());
    }

    if (shouldSave) {
      DEBUGLN("saving config");
      DynamicJsonBuffer jsonBuffer;
      JsonObject& json = jsonBuffer.createObject();
      json["mqtt_server"] = mqtt_server;
      json["mqtt_port"] = mqtt_port;
      json["mqtt_username"] = mqtt_username;
      json["mqtt_password"] = mqtt_password;
      json["device_name"] = device_name;


      File configFile = SPIFFS.open("/config.json", "w");
      if (!configFile) {
        DEBUGLN("failed to open config file for writing");
      }
      #ifdef USESERIAL
      DEBUGLN("");
      json.prettyPrintTo(Serial);
      DEBUGLN("");
      #endif
      json.printTo(configFile);
      configFile.close();
      //end save
    }
}
void setup(){
  #ifdef USESERIAL
  Serial.begin(115200);
  analogWriteRange(255);
  #endif

  loadConfig();
  wifi_station_set_hostname(device_name);
  setupWifi();
  ArduinoOTA.begin();
  led.setChangeStateCallback(sendState);

  strcat(sub_topic, SUB_PREFIX);
  strcat(sub_topic, "/");
  strcat(sub_topic, device_name);
  strcat(pub_topic, PUB_PREFIX);
  strcat(pub_topic, "/");
  strcat(pub_topic, device_name);

  mqttClient.onConnect(onConnect);
  mqttClient.onMessage(onMessage);
  mqttClient.setServer(mqtt_server, atoi(mqtt_port));
  mqttClient.setCredentials(mqtt_username, mqtt_password);
  mqttClient.connect();
}

void loop(){
  button.read();
  ArduinoOTA.handle();

  if (button.pressedFor(5000)){
    WiFi.disconnect(true);
    DEBUGLN("reseting settings");
    delay(1000);
    ESP.restart();
    delay(2000);
  }else if (button.wasReleased()){
    led.toggle();
  }
}
