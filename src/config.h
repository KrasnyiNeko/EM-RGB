#define DEVICE_NAME ""

//WIFI
#define SSID ""
#define WIFI_PASSWORD ""
#define AP_PASSWORD "ESP-8266"

//MQTT
#define PUB_PREFIX "state"
#define SUB_PREFIX "cmnd"
#define SERVER "0.0.0.0"
#define PORT "1883"
#define USERNAME ""
#define PASSWORD ""
#define CMND_ON "ON"
#define CMND_OFF "OFF"

// Uncomment board which you are using. If board you are using is diffrent look
// at the section at the bottom. Serial is disabled on esp01 due to rx/tx pins
// also being gpio pins
#define NODE
//#define ESP01

#define USESERIAL
#define DEBUG


// No real need to go past here
#ifdef NODE
  #define BUTTON_PIN D1
  #define R_PIN D2
  #define G_PIN D3
  #define B_PIN D4
#endif
#ifdef ESP01
  // debug is disabled on esp01 due to pin restrictions
  #define BUTTON_PIN 3
  #define R_PIN 0
  #define G_PIN 1
  #define B_PIN 2
  #undef USESERIAL
#endif

#ifdef USESERIAL
  #define DEBUGLN(x) Serial.println(x)
  #define DEBUGPR(x) Serial.print(x);
#else
  #define DEBUGLN(x)
  #define DEBUGPR(x)
#endif
