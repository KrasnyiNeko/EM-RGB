#include <RGBLed.h>
#include <Arduino.h>

RGBLed::RGBLed(int r, int g, int b){
  r_pin = r;
  g_pin = g;
  b_pin = b;

  pinMode(r_pin, OUTPUT);
  pinMode(g_pin, OUTPUT);
  pinMode(b_pin, OUTPUT);
}

void RGBLed::on(){
  state.on = true;
  set(state);
}

void RGBLed::off(){
  state.on = false;
  if (stateChangeCallback != NULL) stateChangeCallback();
  write(0,0,0,0);
}

void RGBLed::toggle(){
  (state.on) ? off() : on();
}

void RGBLed::set(byte r, byte g, byte b, byte brightness){
  // Save state to memory
  state.red   = r;
  state.green = g;
  state.blue  = b;
  state.brightness = brightness;

  if (stateChangeCallback != NULL) stateChangeCallback();
  if(state.on) write(r, g, b, brightness);
}

void RGBLed::write(byte r, byte g, byte b, byte brightness){
  // Map individual colours to brightness
  byte red   = map(r, 0, 255, 0, brightness);
  byte green = map(g, 0, 255, 0, brightness);
  byte blue  = map(b, 0, 255, 0, brightness);

  // Finally write out to the leds
  analogWrite(r_pin, red);
  analogWrite(g_pin, green);
  analogWrite(b_pin, blue);
}

void RGBLed::set(ledState s){
  set(s.red, s.green, s.blue, s.brightness);
}

void RGBLed::setBrightness(byte brightness){
  set(state.red, state.green, state.blue, brightness);
}

void RGBLed::setColour(byte r, byte g, byte b){
  set(r, g, b, state.brightness);
}

void RGBLed::setChangeStateCallback(void (*func)()){
  stateChangeCallback = func;
}

ledState RGBLed::getState(){
  return state;
}
