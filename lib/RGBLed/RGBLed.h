#include <Arduino.h>
//TODO: analogWriteRange(255); currently needs to be called to set the range for
// the esp analog. By default it goes up to 1023. Possibly change code to auto
// map to 1023

typedef struct {
  byte red   = 255;
  byte green = 255;
  byte blue  = 255;
  byte brightness = 255;
  bool on = false;
} ledState;

class RGBLed{
private:
  int r_pin, g_pin, b_pin;
  ledState state;
  void write(byte r, byte g, byte b, byte brightness);
  void (*stateChangeCallback)(void) = NULL;
public:
  RGBLed(int r, int g, int b);
  void on();
  void off();
  void toggle();
  void set(ledState s);
  void set(byte r, byte g, byte b, byte brightness);
  void setBrightness(byte brightness);
  void setColour(byte r, byte g, byte b);
  void setChangeStateCallback(void (*func)(void));
  ledState getState();
};
